package test;

import java.util.List;

import utils.TextFile;
import core.BaumWelch;
import core.ForwardProcedure;
import core.HMM;
import core.Observation;

public class BaumWelchTester {

	/// Filepaths
	private static final String  obsFilepath = "example2.obs";
	private static final String  hmmFilepath = "sentence.hmm";
///	private static final String  optFilepath = "sentence-opti.hmm";

	/**
	 * Baum Welsh Test Unit
	 */
	public static void main(String[] args)  throws Exception{
		// Load the HMM file and parse
		System.out.println("Loading hmm File "+hmmFilepath);
		String[] hmmFile = TextFile.getLinesAsArray( hmmFilepath);
		System.out.println("Parsing hmm File"+ hmmFilepath);
		HMM hmm = HMM.parseHMM( hmmFile );

		// Load the Obs file and parse
		System.out.println("Loading obs File"+ obsFilepath);
		String[] obsFile = TextFile.getLinesAsArray( obsFilepath );
		System.out.println("Parsing obs File "+ obsFilepath);
		List<Observation> list = Observation.parseObservation( obsFile );
		
		System.out.println("\n Testing Baum Welch Algorithm ");
		// Run Forward Procedure on all three observations and look at the results
		for ( Observation oo: list)
		{
			BaumWelch.evaluate(oo, hmm, 1);
			//System.out.println( hmm.toString() );
			
			Double probability = ForwardProcedure.evaluate(oo, hmm);
			System.out.println(probability);
		}
	}
}
