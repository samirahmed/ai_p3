package test;

import utils.TextFile;
import core.HMM;

/**
 * Test unit for HMM Parser
 * @author Samir Ahmed
 */
public class HMMParser {

	private static final String filepath = "sentence.hmm";

	public static void main(String[] args) throws Exception
	{
		// Load File as String Array
		System.out.println("Loading "+filepath);
		String[] fileLines = TextFile.getLinesAsArray(filepath);
		
		// Parse HMM
		System.out.println("\nAttempting to Parse "+filepath);
		HMM hmm = HMM.parseHMM( fileLines);
		
		// Print out the A matrix
		System.out.println("\nA: ");
		for ( int start = 0; start< hmm.stateCount; start++)
		{
			for ( int end = 0; end < hmm.stateCount ; end++)
			{
				System.out.print(hmm.A(start,end)+" ");
			}
			System.out.println();
		}

		// Print out the B Matrix
		System.out.println("\nB: ");
		for ( int start = 0; start< hmm.stateCount; start++)
		{
			for ( int end = 0; end < hmm.stateCount ; end++)
			{
				System.out.print(hmm.A(start,end)+" ");
			}
			System.out.println();
		}

		// Print out Pi
		System.out.println("\nPi: ");
		for ( int state =0;  state<hmm.stateCount; state++ )
		{
			System.out.print(hmm.Pi(state) + " ");
		}
		System.out.println();

		// Print out STRUCTURE
		System.out.println("\nSTRUCTURE: ");
		for ( String ss: hmm.structure )
		{
			System.out.print(ss+" ");
		}
		System.out.println();

		// Print out VOCABULARY
		System.out.println("\nVOCABULARY: ");
		for ( String ss: hmm.vocabulary )
		{
			System.out.print(ss+" ");
		}
		System.out.println();

		// Test to string method
		System.out.println("\nTesting toString() Method of HMM Class:");
		System.out.println(hmm);

	}

}
