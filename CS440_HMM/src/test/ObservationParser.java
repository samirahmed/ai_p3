package test;

import java.util.List;

import utils.TextFile;
import core.Observation;

/**
 * Test Unit for Observation Class
 * @author Samir Ahmed
 *
 */
public class ObservationParser {

	private final static String filepath = "example1.obs";
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws Exception{

		// Load File as String Array
		System.out.println("Loading "+filepath);
		String[] fileLines = TextFile.getLinesAsArray(filepath);

		// Parse HMM
		System.out.println("\nAttempting to Parse "+filepath);
		List<Observation> list = Observation.parseObservation(fileLines);

		// Print all the observation
		int count = 0;
		for ( Observation oo : list )
		{
			System.out.println("\nObservation Number "+(count+1)+" :");
			for ( int ii=0; ii< oo.length() ; ii++ )
			{
				System.out.print(oo.at(ii)+" ");
			}
			count++;
			System.out.println();
			System.out.println("\nTesting toString method:\n"+oo+"\n");
		}
		


	}

}
