package test;

import java.util.List;

import utils.TextFile;
import core.BackwardProcedure;
import core.ForwardProcedure;
import core.HMM;
import core.Observation;
import core.ViterbiAlgorithm;

/**
 * Test Viterbi
 * @author Samir Ahmed
 */
public class ViterbiTester {

	private static final String  obsFilepath = "question2.obs";
	private static final String  hmmFilepath = "sentence.hmm";

	/**
	 * Test unit
	 */
	public static void main(String[] args) throws Exception{	

		// Load the HMM file and parse
		System.out.println("Loading hmm File "+hmmFilepath);
		String[] hmmFile = TextFile.getLinesAsArray( hmmFilepath);
		System.out.println("Parsing hmm File"+ hmmFilepath);
		HMM hmm = HMM.parseHMM( hmmFile );

		// Load the Obs file and parse
		System.out.println("Loading obs File"+ obsFilepath);
		String[] obsFile = TextFile.getLinesAsArray( obsFilepath );
		System.out.println("Parsing obs File "+ obsFilepath);
		List<Observation> list = Observation.parseObservation( obsFile );


		System.out.println("\n Testing Forward Procedures");
		// Run Forward Procedure on all three observations and look at the results
		for ( Observation oo: list)
		{
			Double probability = ForwardProcedure.evaluate(oo, hmm);
			System.out.println( probability ) ;
		}

		System.out.println("\n Testing Back Procedures");
		// Run Backward Procedure on all three observations and look at the results
		for ( Observation oo: list)
		{
			Double probability = BackwardProcedure.evaluate(oo, hmm);
			System.out.println( probability ) ;
		}
		
		System.out.println("\n Testing the Viterbi Algorithm");
		// Run the Viterbi Algorithm and print the state path
		for ( Observation oo: list)
		{
			List<String> statepath = ViterbiAlgorithm.evaluate(oo, hmm);
			System.out.println( statepath.toString() ) ;
		}

	}

}
