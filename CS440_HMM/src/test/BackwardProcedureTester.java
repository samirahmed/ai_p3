package test;

import java.util.List;

import utils.TextFile;
import core.BackwardProcedure;
import core.HMM;
import core.Observation;

public class BackwardProcedureTester {

	private static final String  obsFilepath = "example1.obs";
	private static final String  hmmFilepath = "sentence.hmm";
	
	public static void main(String[] args) throws Exception{	


		// Load the HMM file and parse
		System.out.println("Loading hmm File "+hmmFilepath);
		String[] hmmFile = TextFile.getLinesAsArray( hmmFilepath);
		System.out.println("Parsing hmm File"+ hmmFilepath);
		HMM hmm = HMM.parseHMM( hmmFile );

		// Load the Obs file and parse
		System.out.println("Loading obs File"+ obsFilepath);
		String[] obsFile = TextFile.getLinesAsArray( obsFilepath );
		System.out.println("Parsing obs File "+ obsFilepath);
		List<Observation> list = Observation.parseObservation( obsFile );

		// Run Forward Procedure on all three observations and look at the results
		for ( Observation oo: list)
		{
			Double probability = BackwardProcedure.evaluate(oo, hmm);
			System.out.println( probability ) ;
		}
	}
}
