package test;

import main.Optimize;

/**
 * Optimization script tester
 * @author Samir Ahmed
 */
public class OptimizeTester {

	public static void main(String[] args) throws Exception
	{
		Optimize.main(new String[]{"sentence.hmm","example2.obs","sentence-opti.hmm"});
	}
	
}
