package test;

import main.Statepath;

/**
 * Test the functionality of the statepath
 * @author Samir Ahmed
 *
 */
public class StatepathTester {

	public static void main(String[] args) throws Exception
	{
		Statepath.main(new String[]{"sentence.hmm","example1.obs"});
	}
}