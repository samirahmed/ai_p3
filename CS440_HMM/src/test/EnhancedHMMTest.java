package test;

import java.util.List;

import utils.TextFile;
import core.ForwardProcedure;
import core.HMM;
import core.Observation;

public class EnhancedHMMTest {

	private static final String  obsFilepath = "sample.obs";
	private static final String  hmmFilepath = "sentence.hmm";
	private static final String  enhancedHMMFilepath = "enhanced.hmm";

	public static void main(String[] args) throws Exception{	

		// Load the HMM file and parse
		System.out.println("Loading hmm File "+hmmFilepath);
		String[] hmmFile = TextFile.getLinesAsArray( hmmFilepath);
		System.out.println("Parsing hmm File"+ hmmFilepath);
		HMM hmm = HMM.parseHMM( hmmFile );

		// Load the HMM file and parse
		System.out.println("Loading hmm File "+enhancedHMMFilepath);
		String[] enhancedHMMFile = TextFile.getLinesAsArray( enhancedHMMFilepath);
		System.out.println("Parsing hmm File"+ enhancedHMMFilepath);
		HMM enhanced = HMM.parseHMM( enhancedHMMFile );

		// Load the Obs file and parse
		System.out.println("Loading obs File"+ obsFilepath);
		String[] obsFile = TextFile.getLinesAsArray( obsFilepath );
		System.out.println("Parsing obs File "+ obsFilepath);
		List<Observation> list = Observation.parseObservation( obsFile );

		// Run Forward Procedure on all three observations and look at the results
		System.out.println( "Original\t| Enhanced" ) ;
		for ( Observation oo: list)
		{
			Double probability = ForwardProcedure.evaluate(oo, hmm);
			Double probabilityEnhanced = ForwardProcedure.evaluate(oo, enhanced);
			System.out.println( probability + "\tvs\t" + probabilityEnhanced) ;
		}
	}

}
