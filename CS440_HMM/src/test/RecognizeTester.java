package test;

import main.Recognize;

/**
 * Test the functionality of the recognizer
 * @author Samir Ahmed
 *
 */
public class RecognizeTester {

	public static void main(String[] args) throws Exception
	{
		Recognize.main(new String[]{"sentence.hmm","example1.obs"});
	}
}