package core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import utils.TextTools;

/**
 * Observation Class: Contains Observation Data
 * @author Samir Ahmed
 */
public class Observation {
	
	private ArrayList<String> data;
	public Double[][] 	ALPHA;
	public Double[][] 	BETA;
	public Double[][] 	DELTA;
	public Double[][]	GAMMA;
	public Double[][]	KSI;
	public int[][] 		PSI;
	public Double 		PROBABILITY;
	public Double 		PMAX;
	public int[]		PATH;
	
	/**
	 * Create an observation from an array of Strings
	 * @param observations	Array of Strings
	 */
	public Observation(String [] observations){
		
		// Load the observation
		this.data = new ArrayList<String> (); 
		Collections.addAll(this.data, observations);
		
		// Initialize the path to all impossible numbers, until set by Viterbi Algorithm
		this.PATH = new int[this.data.size()];
		for (int ii =0; ii<this.PATH.length ;ii++)
		{
			this.PATH[ii]=-1;
		}

	}
	
	/**
	 * Get the size of the observation
	 * @return	Integer value
	 */
	public int length()
	{
		return this.data.size();
	}
	
	/**
	 * Get an Element from the Observation data, given an index to it
	 * @param index	
	 * @return	String observation item
	 */
	public String at(int index)
	{
		return this.data.get(index);
	}
	
	/**
	 * String Object of the Output Observation
	 * @return String containing the Output observation Sequence
	 */
	public String toString()
	{
		return data.toString();
	}
	
	/**
	 * Makes string formatted alpha Matrix
	 * @return	A string representation of ALPHA
	 */
	public String AlphaMatrix()
	{
		StringBuilder sb =  new StringBuilder();
		for ( int ii=0; ii< ALPHA.length ; ii++)
		{
			for ( int jj=0; jj< ALPHA[ii].length ; jj++ )
			{
				sb.append(ALPHA[ii][jj]+" ");
			}
			sb.append('\n');
		}
		
		return sb.toString();
	}
	
	/**
	 * Makes string formatted beta Matrix
	 * @return	A string representation of BETA
	 */
	public String BetaMatrix()
	{
		StringBuilder sb =  new StringBuilder();
		for ( int ii=0; ii< BETA.length ; ii++)
		{
			for ( int jj=0; jj< BETA[ii].length ; jj++ )
			{
				sb.append(BETA[ii][jj]+" ");
			}
			sb.append('\n');
		}
		
		return sb.toString();
	}
	
	/**
	 * Makes string formatted beta Matrix
	 * @return	A string representation of BETA
	 */
	public String DeltaMatrix()
	{
		StringBuilder sb =  new StringBuilder();
		for ( int ii=0; ii< DELTA.length ; ii++)
		{
			for ( int jj=0; jj< DELTA[ii].length ; jj++ )
			{
				sb.append(DELTA[ii][jj]+" ");
			}
			sb.append('\n');
		}
		
		return sb.toString();
	}
	
	/**
	 * parse Observations form a .obs file and return a list of observations
	 * @param file	An Array list with .obs split by new lines	
	 * @return	 A List of Observations
	 */
	public static List<Observation> parseObservation(String [] file)
	{
		int line =0;
		
		// Get the count from the first line
		int count = TextTools.readIntegers(file[line])[0];
		line++;
		
		// Create a list container for parsing observations
		ArrayList<Observation> list =  new ArrayList<Observation> (file.length); 
		
		for ( int ii=0; ii< count; ii++)
		{
			line++; // SKIP the count line
			list.add(new Observation (TextTools.readString(file[line])));
			line++; // Move to next count line
		}
		
		return list;
	}
}
