package core;


/**
 * Backward Procedure wrapper class
 * 
 * @author Samir Ahmed
 */
public class BackwardProcedure {

	/**
	 * Perform the Backward Procedure on a given HMM and observation oo
	 * 
	 * @param oo	Observation Sequence
	 * @param hmm	HMM
	 * @return		The probability of seeing the observation sequence 
	 */
	public static Double evaluate( Observation oo, HMM hmm ){

		// Intialize the BETA Matrix as a property of the output observation oo
		createBetaMatrix(oo,hmm);
		int LAST = oo.length()-1;

		// --------------------------
		// STEP 1: Problem Initialize
		// --------------------------	

		// For all states, at t=FINAL we have a fixed probability of seen the output 
		for ( int state = 0; state< hmm.stateCount ;state++ )
		{
			oo.BETA[state][LAST]= 1.0; 
		}

		// ----------------------------------------------------
		// STEP 2: Recursive Relation : Induction
		// ----------------------------------------------------

		// For every time step, from the second last down to the first
		for ( int time = (LAST-1); time>=0 ; time-- )
		{
			// We look at every possible previous state ii
			for ( int ii =0; ii< hmm.stateCount ; ii++)
			{
				// Calculate the Transition Sum
				Double TransitionSum = 0.0;
				for( int jj=0; jj< hmm.stateCount ; jj++)
				{
					Double Aij	= hmm.A(ii,jj);
					Double Bj	= hmm.B(jj, oo.at(time+1));
					Double Betaj= oo.BETA[jj][time+1];
					TransitionSum += Aij * Bj * Betaj; 
				}
				
				// Store the BETA value
				oo.BETA[ii][time] = TransitionSum;
			}
		}
		
		
		// ----------------------------------
		// STEP 3: Termination : Calculation
		// -----------------------------------	
		
		// Given our BETA Matrix, we can find the probabilities at Time=0 
		Double probability = 0.0;
		
		for ( int ii=0; ii< hmm.stateCount ; ii++ )
		{
			Double b1  	= hmm.B(ii, oo.at(0) );
			Double Beta1= oo.BETA[ii][0];
			Double pi1	= hmm.Pi(ii);
			probability += Beta1 * pi1 *  b1;
		}
		
		return probability;
		


	}

	/**
	 * Creates a BETA Matrix as a property of an Observation
	 * Initializes everything to zeros
	 * @param oo	Observation
	 * @param hmm	HMM
	 */
	private static void createBetaMatrix( Observation oo, HMM hmm ){
		oo.BETA = new Double[hmm.stateCount][ oo.length() ];

		// Initialize the ALPHA matrix to zeros;
		for (int state = 0; state < hmm.stateCount ; state++){
			for (int tt=0; tt< oo.length() ; tt++){
				oo.BETA[state][tt]= 0.0;
			}
		}
	}
}
	