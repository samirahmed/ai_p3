package core;

import utils.Special;

/**
 * Baum-Welch Algorithm Static Wrapper Class
 * @author Jeff Crowell, Samir Ahmed
 */
public class BaumWelch {
	
	/**
	 * Runs the Baum Welch Update
	 * @param oo	Output Observation Object for training
	 * @param hmm	HMM to be learned
	 * @param steps	Number of times to run BW Update function
	 */
	public static void evaluate( Observation oo, HMM hmm , int steps)
	{
		// Local Data Structures
		Double piUpdate[] = new Double [hmm.stateCount];
		Double aUpdate[][] = new Double [hmm.stateCount][hmm.stateCount];
		Double bUpdate[][] = new Double [hmm.stateCount][hmm.observationCount];
		createGammaMatrix(oo,hmm);
		for (int ss=0; ss<steps; ss++)
		{
			// Recreate ALPHA, BETA,  GAMMA
			BackwardProcedure.evaluate(oo, hmm);
			ForwardProcedure.evaluate(oo, hmm);
			updateGammaMatrix(oo, hmm);

			//--------------------
			// PI Update Function
			//--------------------

			for(int ii = 0; ii < hmm.stateCount; ii++)
			{
				piUpdate[ii] = oo.GAMMA[ii][0]; //get the state at time zero
			}

			//--------------------------------
			// A (Transition) Update Function
			//--------------------------------

			for(int ii = 0; ii < hmm.stateCount; ii++)
			{
				for(int jj = 0; jj < hmm.stateCount; jj++)
				{
					Double numerator 	= 0.0;
					Double denominator 	= 0.0;
					for (int time=0; time< (oo.length()-1); time++ ) 
					{
						// SUM [ KSI_{t}(ii,jj)] from First to SECOND LAST
						numerator	+= calcKSI( oo,hmm, time, ii, jj );

						// SUM [ GAMMA_{y}(ii)] from first to SECOND LAST
						denominator	+= oo.GAMMA[ii][time];
					}

					aUpdate[ii][jj] = Special.div(numerator,denominator);
				}
			}

			//--------------------------------
			// B (Output) Update Function
			//--------------------------------

			for ( int jj = 0; jj< hmm.stateCount ; jj++)
			{
				for ( int kk = 0; kk < hmm.observationCount ; kk++)
				{
					Double numerator 	= 0.0;
					Double denominator 	= 0.0;
					for ( int time=0; time < oo.length() ;time++)
					{
						if ( oo.at(time).equals(hmm.vocabulary.get(kk)))
						{
							numerator += oo.GAMMA[jj][time];
						}
						//numerator += oo.GAMMA[jj][time] * (oo.at(time).equals(hmm.vocabulary.get(kk)) ? 1 : 0);
								
						denominator += oo.GAMMA[jj][time];
					}

					// Fill out the new updated B Matrix
					bUpdate[jj][kk] = Special.div(numerator, denominator) ;		
				}
			}

			//-------------------
			// Update HMM Object 
			//-------------------

			// Update the PI vector
			for ( int ii=0; ii<hmm.stateCount ;ii++)
			{
				hmm.Piset( ii, piUpdate[ii] );
			}

			// Update the A matrix
			for ( int ii=0; ii<hmm.stateCount ;ii++)
			{
				for(int jj=0; jj<hmm.stateCount; jj++)
				{
					hmm.Aset(ii, jj, aUpdate[ii][jj] );
				}
			}

			// Update the B matrix
			for ( int jj=0; jj<hmm.stateCount ;jj++)
			{
				for(int kk=0; kk<hmm.stateCount; kk++)
				{
					hmm.Bset( jj, kk , bUpdate[jj][kk] );
				}
			}

		}

	}

	/**
	 * Calculate KSI given time step from STATE II and TO STATE JJ
	 * @param oo 	The Observation
	 * @param hmm 	The hidden markov model
	 * @param time 	The time T
	 * @param fromState the state FROM
	 * @param toState the state TO
	 * @return KSI Probability of a given state transitions in a given time fram
	 */
	private static Double calcKSI(Observation oo, HMM hmm, int time, int fromState, int toState)
	{
		//first, we'll get the numerator
		Double numerator = 1.0;
		numerator *= oo.ALPHA[fromState][time];
		numerator *= oo.BETA[toState][time+1];

		// Make the numerator :  a_{ij}b_{j}(O(_{t+1}))
		numerator *= hmm.A(fromState, toState);
		numerator *= hmm.B(toState, oo.at(time+1)); 

		Double denominator = 0.0;

		// For Every possible starting state
		for(int ii=0; ii < hmm.stateCount; ii++)
		{
			// For Every possible Ending state
			for(int jj = 0; jj < hmm.stateCount; jj++)
			{
				Double product = 1.0;
				product *= oo.ALPHA[ii][time];
				product *= oo.BETA[jj][time+1];
				product *= hmm.A(ii, jj);
				product *= hmm.B(jj, oo.at(time+1));

				denominator += product;
			}
		}

		return Special.div(numerator ,denominator);
	}

	/**
	 * Populate the GAMMA MATRIX
	 * @param oo
	 * @param hmm
	 */
	private static void updateGammaMatrix( Observation oo, HMM hmm ){
		// Add the GAMMA Array to this observation
		//createGammaMatrix(oo, hmm);

		// Initialize the GAMMA matrix to zeros;
		for (int state = 0; state < hmm.stateCount ; state++){
			for (int tt=0; tt< oo.length() ; tt++){

				// Fill GAMMA with computed values
				oo.GAMMA[state][tt]= computeGamma(state, tt, oo, hmm);
			}
		}
	}
	
	/**
	 * Creates a gamma matrix in the observation oo
	 * @param oo	Observation output object
	 * @param hmm	hmm
	 */
	public static void createGammaMatrix( Observation oo, HMM hmm )
	{
		oo.GAMMA = new Double[hmm.stateCount][ oo.length() ];
	}

	/**
	 * Computes gamma for a given state at given time step
	 * 
	 * @param ii	STATE
	 * @param TT	TIME STEP
	 * @param oo	OUTPUT OBSERVATION OBJECT
	 * @param hmm	HMM
	 * @return		Probability that we are in a given state at a given time
	 */
	private static double computeGamma(int ii, int TT, Observation oo, HMM hmm)
	{
		//As given in the Rabiner paper, eqn 27:
		//first, get the numerator
		Double alphaTi = oo.ALPHA[ii][TT];
		Double betaTi = oo.BETA[ii][TT];
		Double numerator = alphaTi * betaTi;

		//Denominator is the sum of Alpha and Beta for all States
		Double denominator = 0.0;
		for(int kk = 0; kk < hmm.stateCount; kk++)
		{
			denominator += (oo.ALPHA[kk][TT] * oo.BETA[kk][TT]);
		}
		return Special.div(numerator, denominator);
	}

}
