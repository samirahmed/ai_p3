package core;

/**
 * Forward Procedure Function Wrapper class
 * @author Samir Ahmed
 */
public class ForwardProcedure  {

	/**
	 * Static Function for evaluating the forward procedure on an output observation given a HMM
	 * 
	 * @param oo	Ouput observatio
	 * @param hmm	Hidden Markov Model
	 * @return		P(OutputSequence|HMM)
	 */
	public static Double evaluate( Observation oo, HMM hmm ){
		
		// Create an ALPHA Matrix as a property of the Observation
		createAlphaMatrix(oo, hmm);
		
		// --------------------------
		// STEP 1: Problem Initialize
		// --------------------------	

		// For all states, at t=0 we have a fixed probability of seen the output 
		for ( int state = 0; state< hmm.stateCount ;state++ )
		{
			oo.ALPHA[state][0]= hmm.Pi(state)*hmm.B(state, oo.at(0) );
		}

		// ----------------------------------------------------
		// STEP 2: Recursive Relation : Induction
		// ----------------------------------------------------
		
		// For every time step, after the initial to the end
		for ( int time = 1; time< oo.length() ; time++ )
		{
			// We look at every possible state
			for ( int jj =0; jj< hmm.stateCount ; jj++)
			{
				// Calculate the probability of transitioning to this state from the previous step in time
				Double TransitionSum = 0.0;
				for( int ii=0; ii< hmm.stateCount ; ii++)
				{
					TransitionSum += oo.ALPHA[ii][time-1]* hmm.A(ii,jj);
				}
				
				// Calculate B_j(O_t+1) as the output Probability
				Double Bj = hmm.B(jj, oo.at(time) );
				
				// Multiply my Bj and Tranisition Sum Probabilities and store in the current state and time
				oo.ALPHA[jj][time] = TransitionSum * Bj ;
			}
		}
		
		// ----------------------------------
		// STEP 3: Termination : Calculation
		// -----------------------------------	
		
		// Give our HMM, we can sum the probaility of every state
		
		Double probability = 0.0;
		int last = oo.length()-1;
		for ( int ii=0; ii< hmm.stateCount ; ii++ )
		{
			probability += oo.ALPHA[ii][ last ];
		}
	
		// Set the oo.PROBABILITY value
		oo.PROBABILITY= probability;
		
		return probability;
		
	}	

	/**
	 * Create an ALPHA MATRIX in the observation object.
	 * The matrix is of size STATE-COUNT x TIME
	 * @param oo	Output Observation
	 * @param hmm	Corresponding HMM
	 */
	private static void createAlphaMatrix( Observation oo, HMM hmm ){
		// Add the ALPHA Array to this observation
		oo.ALPHA = new Double[hmm.stateCount][ oo.length() ];

		// Initialize the ALPHA matrix to zeros;
		for (int state = 0; state < hmm.stateCount ; state++){
			for (int tt=0; tt< oo.length() ; tt++){
				oo.ALPHA[state][tt]= 0.0;
			}
		}
	}

}

