package utils;

public class Special {

	/**
	 * Special Divide will return 0 for 0/0
	 */
	public static Double div(Double numerator, Double denominator)
	{
		if (numerator.equals(0.0))
		{
			return 0.0;
		}
		else
		{
			return numerator/denominator;
		}
	}
	
}
