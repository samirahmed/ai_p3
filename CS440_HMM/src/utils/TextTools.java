package utils;

/**
 * 
 * Text Tools is a class of static functions for dealing with simple and mundane String Manipulation and Tokenization Tools
 * 
 * @author Samir Ahmed, Jeff Crowell
 */
public class TextTools {

	/**
	 * Tokenizes white space delimited strings
	 * @param line Of Words Separated by whitespace characters
	 * @return	An array of individual words
	 */
	public static String[] readString(String line)
	{
		// Tokenize the line
		String[] words = line.split("\\s");
		return words;
	}

	/**
	 * Reads integers from a string of white space delimited integers
	 * @param A string with int values separated by spaces
	 * @return	An int array of parsed values
	 */
	public static int[] readIntegers(String line)
	{
		// Tokenize the line
		String[] numStrings = line.split("\\s");
		int [] nums = new int[numStrings.length];

		// Parse each string as an integer
		int index = 0;
		for ( String ss:  numStrings)
		{
			nums[index] = Integer.parseInt(ss);
			index++;
		}
		return nums;

	}

	/**
	 * Reads an integer from a given line
	 * @param line	A string with white space delimited doubles
	 * @return An array of parsed Doubles
	 */
	public static Double[] readDoubles(String line)
	{
		// Tokenize the line
		String[] numStrings = line.split("\\s");
		Double [] nums = new Double[numStrings.length];

		// Parse each string as an integer
		int index = 0;
		for ( String ss:  numStrings)
		{
			nums[index] = Double.parseDouble(ss);
			index++;
		}

		return nums;

	}	
	
	/**
	 * Simple splitter - Simply takes a string and splits into chunks of specified size
	 * Taken from Jon Skeet 
	 * @param text	A String object to be split
	 * @param size	A desired chunk size
	 * @return		An array list of Strings of chunk size
	 */
	public static String[] splitEqually(String text, int size){

		String[] ret = new String[(text.length() + size - 1) / size];

		int ii= 0;
		for (int start = 0; start < text.length(); start += size, ii++) {
			ret[ii]= (text.substring(start, Math.min(text.length(), start + size)));
		}
		return ret;
	}
	
}
