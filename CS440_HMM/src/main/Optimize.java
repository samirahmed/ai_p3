package main;

import java.util.List;

import utils.TextFile;
import core.BaumWelch;
import core.ForwardProcedure;
import core.HMM;
import core.Observation;

/**
 * Given an HMM, we attempt to learn the HMM via BAUM WELCH ALGORITHM, thus optimizing the probability of observing a sequence 
 * @author Samir Ahmed
 */
public class Optimize {

	public static void main(String[] args) throws Exception {

		// Ensure we have the correct number of arguments
		if ( args.length != 3)
		{
			System.err.println("Invalid Arguments!: Usage: <.hmm file> <.obs file> <output.file>");
			return ;
		}

		// Load the HMM file and parse
		String[] hmmFile = TextFile.getLinesAsArray( args[0] );
		HMM hmm = HMM.parseHMM( hmmFile );

		// Load the Obs file and parse
		String[] obsFile = TextFile.getLinesAsArray( args[1] );
		List<Observation> list = Observation.parseObservation( obsFile );

		// Run Forward Procedure on all three observations and look at the results
		for ( Observation oo: list)
		{
			Double before = ForwardProcedure.evaluate(oo, hmm);
			BaumWelch.evaluate(oo, hmm, 1);
			Double after = ForwardProcedure.evaluate(oo, hmm);
			
			System.out.println( String.format("%.6f",before) + "\t" + String.format("%.6f",after) );
		}
		
		// Write the hmm to file
		TextFile.write("", args[2], hmm.toString());

	}

}
