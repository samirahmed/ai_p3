package main;

import java.util.List;

import core.ForwardProcedure;
import core.HMM;
import core.Observation;

import utils.TextFile;

/**
 *  
 * Recognizer class: This is an entry point for running the recognize script <br/>
 * The Recognizer builds an HMM and observation Outputs, then runs a forward procedure using the Output Sequence and HMM 
 * @author Samir Ahmed
 */
public class Recognize {

	public static void main(String [] args) throws Exception
	{
		
		// Ensure we have the correct number of arguments
		if ( args.length != 2)
		{
			System.err.println("Invalid Arguments!: Usage: <.hmm file> <.obs file>");
			return ;
		}
		
		// Load the HMM file and parse
		String[] hmmFile = TextFile.getLinesAsArray( args[0] );
		HMM hmm = HMM.parseHMM( hmmFile );
		
		// Load the Obs file and parse
		String[] obsFile = TextFile.getLinesAsArray( args[1] );
		List<Observation> list = Observation.parseObservation( obsFile );
		
		// Run Forward Procedure on all three observations and look at the results
		for ( Observation oo: list)
		{
			Double probability = ForwardProcedure.evaluate(oo, hmm);
			System.out.println( probability ) ;
		}
	}
	
}
