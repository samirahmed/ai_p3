package main;

import java.util.List;

import utils.TextFile;
import core.ForwardProcedure;
import core.HMM;
import core.Observation;
import core.ViterbiAlgorithm;

/**
 * Given a .hmm file and .obs file: Statepath is perform the Viterbi Algorithm to trace the most likely state path 
 * @author Samir Ahmed
 */
public class Statepath {

	public static void main(String[] args) throws Exception {


		// Ensure we have the correct number of arguments
		if ( args.length != 2)
		{
			System.err.println("Invalid Arguments!: Usage: <.hmm file> <.obs file>");
			return ;
		}

		// Load the HMM file and parse
		String[] hmmFile = TextFile.getLinesAsArray( args[0] );
		HMM hmm = HMM.parseHMM( hmmFile );

		// Load the Obs file and parse
		String[] obsFile = TextFile.getLinesAsArray( args[1] );
		List<Observation> list = Observation.parseObservation( obsFile );

		// Run Forward Procedure on all three observations and look at the results
		for ( Observation oo: list)
		{
			// Run forward procedure
			Double probability = ForwardProcedure.evaluate(oo, hmm);
			
			// Print probability
			System.out.print( probability+"\t" ) ;
			
			if ( !probability.equals(0.0))
			{
				// Run backward procedure
				List<String> statepath = ViterbiAlgorithm.evaluate(oo, hmm);
				for( String ss : statepath ) System.out.print(ss + " ");	
			}
			
			System.out.println();
		}


	}

}
